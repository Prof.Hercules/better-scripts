import { readFileSync } from "fs";

import { defineConfig } from "tsup";

const pkg = JSON.parse(readFileSync("package.json").toString());

const external = [
  ...Object.keys(pkg.dependencies || {}),
  ...Object.keys(pkg.devDependencies || {}),
  ...Object.keys(pkg.peerDependencies || {}),
  ...Object.keys(pkg.bundledDependencies || {}),
  ...Object.keys(pkg.optionalDependencies || {}),
];

export default defineConfig({
  entry: ["src/index.ts"],
  splitting: false,
  sourcemap: false,
  clean: true,
  dts: true,
  format: "esm",
  target: "node16",
  bundle: true,
  minify: false,
  external: [],
});
