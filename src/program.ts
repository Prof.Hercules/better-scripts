import { Command, Option } from "commander";

import type { ScriptOption, BetterScriptConfig } from "./types";

export const runCli = (rootConfig: BetterScriptConfig) => {
  const program = new Command();
  const cli = program
    .name("better-scripts")
    .description("Supercharge your npm scripts")
    .version("0.0.3")
    .option("-y, --confirm", "Skip confirmation prompt")
    .option("--dryRun", "print out commands without executing anything")
    .option("--verbose", "print out parsed args before execution");

  let options: Record<string, any> = {};

  const makeOption = (config: ScriptOption): Option => {
    const option = new Option(config.flags);
    option.makeOptionMandatory(true);
    if (config.description) option.description = config.description;
    return option;
  };

  const makeCommand = (name: string, config: BetterScriptConfig[string]) => {
    const command = new Command(name).description(`runs ${name}`);

    if (typeof config === "object") {
      if (config.description && typeof config.description === "string") {
        command.description(config.description);
      }

      if ("script" in config) {
        if (config.options) {
          const opts = Object.values<ScriptOption>(config.options as any);
          for (const optionConfig of opts) {
            command.addOption(makeOption(optionConfig));
          }
        }

        command.action((args) => {
          const keyExists = Object.keys(args).some((k) => k in options);
          if (keyExists) throw new Error("Duplicate command option detected!");
          options = { ...options, ...args };
        });
      } else {
        for (const [sub, _config] of Object.entries(config)) {
          command.addCommand(makeCommand(sub, _config));
        }
      }
    }
    return command;
  };

  for (const [name, config] of Object.entries(rootConfig)) {
    cli.addCommand(makeCommand(name, config));
  }

  const parseResult = cli.parse(process.argv.slice(2), { from: "user" });
  return { args: parseResult.args, options, globals: parseResult.optsWithGlobals() };
};
