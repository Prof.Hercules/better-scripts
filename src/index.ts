#!/usr/bin/env node
import { exec } from "child_process";
import inquirer from "inquirer";
const prompt = inquirer.createPromptModule();

import { runCli } from "./program";
import { scriptSchema } from "./types";
import { getRootConfig, getValueFromPath } from "./utils";

import type { Script } from "./types";

const rootConfig = getRootConfig();
const { args, options, globals } = runCli(rootConfig);
if (globals.verbose) {
  console.group("verbose");
  if (globals.dryRun) console.log("- dryRun enabled, not executing anything");
  console.log({ args, options, globals });
  console.groupEnd();
}

const asyncExec = (command: string) => {
  if (globals.dryRun) {
    console.log("[dryRun]", command);
    return Promise<void>.resolve();
  }
  const child = exec(command);
  child.stdout?.pipe(process.stdout);
  child.stderr?.pipe(process.stderr);
  return new Promise<void>((resolve, reject) => {
    child.addListener("error", () => reject());
    child.addListener("exit", (code) => (code === 0 ? resolve() : reject()));
  });
};

const parseScriptStep = (step: Script): string[] => {
  if (typeof step === "string") {
    return [step];
  }

  if (typeof step.script === "string") {
    return [step.script];
  }

  return step.script;
};

const confirmCommands = async (commands: string[]) => {
  if (globals.confirm) return;
  console.group(`Commands [${commands.length}]:`);
  commands.forEach((c) => console.log(c));
  console.groupEnd();
  const answers = await prompt([
    {
      type: "confirm",
      name: "confirm",
      message: "Are you sure you want to run this script?",
      default: false,
    },
  ]);
  if (!answers.confirm) {
    console.log("Aborting");
    process.exit(0);
  }
};

const runScript = async (script: Script) => {
  const steps = parseScriptStep(script);

  const transformedSteps = steps.reduce((acc, step) => {
    const transformedStep = Object.keys(options ?? {}).reduce((acc, opt) => {
      if (!options || !(opt in options)) return acc;
      return acc.replaceAll(`$${opt}`, options[opt]);
    }, step);

    acc.push(transformedStep);
    return acc;
  }, [] as string[]);

  await confirmCommands(transformedSteps);

  return Promise.all(transformedSteps.map((step) => asyncExec(step)));
};

async function main() {
  await runScript(getValueFromPath(args, rootConfig, scriptSchema));
}

main().catch(console.error);
