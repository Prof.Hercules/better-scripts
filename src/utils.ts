import { cosmiconfigSync } from "cosmiconfig";

import {
  BetterScriptConfigRef,
  betterScriptConfigSchema,
  betterScriptConfigSchemaRef,
  refRegex,
} from "./types";

import type { BetterScriptConfig } from "./types";
import { z } from "zod";

export const getValueFromPath = <T>(path: string[], config: any, schema: z.ZodSchema<T>): T => {
  let result = config;

  for (const key of path) {
    if (key in result) {
      result = result[key] as any;
    } else {
      const parseResult = schema.safeParse(result);
      if (!parseResult.success) throw new Error("Invalid path!");
      return parseResult.data;
    }
  }

  return schema.parse(result);
};

const getRootConfigRef = (): BetterScriptConfigRef => {
  const discoveredConfig = cosmiconfigSync("better-scripts").search();
  if (!discoveredConfig) {
    throw new Error("No betterscript config found!");
  }

  return betterScriptConfigSchemaRef.parse(discoveredConfig.config);
};

const _resolveRefs = (jsonString: string, history: string[] = []): string => {
  const matches = [...jsonString.matchAll(refRegex)];
  const refs = matches.map(([ref]) => ref);

  return refs.reduce((conf, ref) => {
    const path = ref.slice(2, -1).split(":");
    const value = getValueFromPath(path, getRootConfigRef(), z.any());
    const strValue: string = JSON.stringify(value);

    const result = conf.replaceAll(ref, strValue).trim();

    if (strValue.match(refRegex)) {
      if (history.includes(ref)) {
        console.log(history.join(" -> "));
        throw new Error("Circular reference detected!");
      }
      history.push(ref);
      return _resolveRefs(result, history);
    }

    history.push(ref);
    return result;
  }, jsonString);
};

const resolveReferences = (config: BetterScriptConfigRef): BetterScriptConfig => {
  const strConfig = JSON.stringify(config);

  const resolvedConfig = JSON.parse(_resolveRefs(strConfig));

  const parseResult = betterScriptConfigSchema.safeParse(resolvedConfig);

  if (!parseResult.success) {
    parseResult.error.errors.map((e) => console.error({ path: e.path, message: e.message }));
    throw new Error("Invalid config");
  }

  return parseResult.data;
};

export const getRootConfig = (): BetterScriptConfig => resolveReferences(getRootConfigRef());
