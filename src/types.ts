import { z } from "zod";

import type { ConcurrentlyOptions } from "concurrently";

export const scriptOptionSchema = z.object({
  description: z.string().optional(),
  flags: z.string(),
});
export type ScriptOption = z.infer<typeof scriptOptionSchema>;

export const scriptSchema = z.union([
  z.string(),
  z.object({
    description: z.string().optional(),
    options: z.record(scriptOptionSchema).optional(),
    concurrentlyOptions: z
      .custom<ConcurrentlyOptions & Partial<{ group: boolean; colors: boolean }>>()
      .optional(),
    script: z.union([z.string(), z.array(z.string())]),
  }),
]);
export type Script = z.infer<typeof scriptSchema>;

export interface BetterScriptConfig {
  [k: string]: Script | BetterScriptConfig;
}
export const betterScriptConfigSchema: z.ZodType<BetterScriptConfig> = z.lazy(() =>
  z.record(z.union([scriptSchema, betterScriptConfigSchema])),
);

// match any valid json string that starts with a ^
export const refRegex = new RegExp(/"\^(?:[^"\\\t\n\r\^]|\\.)*"/g);
export const scriptOptionSchemaRef = z.union([z.string().startsWith("^"), scriptOptionSchema]);
export type ScriptOptionRef = z.infer<typeof scriptOptionSchemaRef>;

export const scriptSchemaRef = z.union([
  z.string().startsWith("^"), // ref
  z.string(),
  z.object({
    description: z.string().optional(),
    options: z.record(scriptOptionSchemaRef).optional(),
    concurrentlyOptions: z
      .custom<ConcurrentlyOptions & Partial<{ group: boolean; colors: boolean }>>()
      .optional(),
    script: z.union([
      z.string().startsWith("^"), // ref,
      z.string(),
      z.array(z.string()),
    ]),
  }),
]);
export type ScriptRef = z.infer<typeof scriptSchemaRef>;

export interface BetterScriptConfigRef {
  [k: string]: ScriptRef | BetterScriptConfigRef;
}
export const betterScriptConfigSchemaRef: z.ZodType<BetterScriptConfigRef> = z.lazy(() =>
  z.record(z.union([scriptSchemaRef, betterScriptConfigSchemaRef])),
);
